# displayrws

Dit project beschrijft hoe een 1,3 inch HD_IPS_TFT_LCD display van Joy-IT aangesloten kan worden op een ESP32.

## Benodigdheden



- ESP32 (type ESP-WROOM-32) bijv. van Joy-IT
- kabeltjes
- micro USB kabel
- Display 240x240 pixel (SBC-LCD01)

Verder is een computer met Python nodig.
Toegang tot het USB device is noodzakelijk.


## Stappenplan

### 1. Installeer Python

  #### Windows

  Klik op de Windows knop (Start).

  Type in:

  ```cmd```

  vervolgens:

  ```curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py```

  vervolgens:

  ```python get-pip.py```

  Wanneer Python nog niet geinstalleerd is, verschijnt er een Windows Store dialoog.
  In dit Windows Store dialoog, klik op Downloaden van Python.


  #### Voor Linux:

  ```sudo apt install python3```
  ```sudo apt install python3-pip```


### 2. Installeer esphome
  ```pip install esphome```
  ```pip install pillow==10.2.0```
  ```pip install python-magic-bin``` (voor Windows)

  In Windows:
  wanneer deze melding te zien is:

  ```WARNING: The script esphome.exe is installed in C:\Users\<username>\AppData\Local\Packages\PythonSoftwareFoundation.Python..3.12_\LocalCache...```

  Voer dan uit:
  ```setx path "%path%;C:\your\path\here\"```

  Vervang C:\your\path\here\ door het path in de warning: C:\Users\<username>\AppData\Local\Packages\PythonSoftwareFoundation.Python....

  Sluit het Opdrachtprompt en start een nieuw Opdrachtprompt.

### 3. Zorg ervoor dat er genoeg rechten zijn om van het USB apparaat:

  Linux:

  ```sudo usermod -a -G dialout [gebruikersnaam]```

  vervang: [gebruikersnaam] met jouw gebruikersnaam.

  Restart jouw computer of log opnieuw in om deze rechten te effectueren.

### 4. Verbind met de kabeltjes van het ESP32 development board met het display:

  SDA -> D13

  SCLK -> D14

  DC -> D19

  BLK -> D21 (optioneel)

  RES -> D23

  GND -> GND (pin boven 3.3V)

  3V3 -> 3.3V (pin rechtsonder)

![alt text](figs/pinouth.png)

### 5. Verbind de USB kabel met het ESP32 board.

  Het rode lampje moet branden.

### 6. In de USB devices controleer het USB ap

In Linux:
  ```ls /dev```

meestal staat er een nieuw USB device genaamd: ttyUSB01 of ttyUSB02 bij.

### 7. Voer uit:

  ```git clone https://gitlab.com/rwsdatalab/public/codebase/tools/displayrws.git```
  ```cd displayrws```

### 7. Voer uit:

  ```esphome run spi-display-image.yaml```

Wanneer wordt gevraagd hoe te verbinden met het de ESP32 selecteer dan het USB apparaat bijv. /dev/USB01

### 8. Nadat het flashen van de chip klaar is verschijnt er een blauwe vierkant met de tekst: RWS Datalab.

### 9. Maak bijv. een cirkel.

Gebruik deze handleiding:
https://esphome.io/components/display/index.html

## Handleiding ESP32

https://joy-it.net/files/files/Produkte/SBC-NodeMCU-ESP32/SBC-NodeMCU-ESP32-Manual-2021-06-29.pdf

## Handleiding display

https://www.conrad.nl/nl/p/joy-it-joy-it-displaymodule-3-3-cm-1-3-inch-240-x-240-pixel-incl-sbc-opname-2346746.html?refresh=true
